package com.commit451.gitlab.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.commit451.easycallback.EasyCallback;
import com.commit451.gitlab.App;
import com.commit451.gitlab.R;
import com.commit451.gitlab.model.api.Build;
import com.commit451.gitlab.model.api.MergeRequest;
import com.commit451.gitlab.model.api.Milestone;
import com.commit451.gitlab.model.api.Project;
import com.commit451.gitlab.model.api.RepositoryCommit;
import com.commit451.gitlab.navigation.Navigator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Intermediate activity when deep linking to another activity and things need to load
 */
public class LoadSomeInfoActivity extends AppCompatActivity {

    private static final String EXTRA_LOAD_TYPE = "load_type";
    private static final String EXTRA_PROJECT_NAMESPACE = "project_namespace";
    private static final String EXTRA_PROJECT_NAME = "project_name";
    private static final String EXTRA_COMMIT_SHA = "extra_commit_sha";
    private static final String EXTRA_MERGE_REQUEST = "merge_request";
    private static final String EXTRA_BUILD_ID = "build_id";
    private static final String EXTRA_MILESTONE_ID = "milestone_id";

    private static final int LOAD_TYPE_DIFF = 0;
    private static final int LOAD_TYPE_MERGE_REQUEST = 1;
    private static final int LOAD_TYPE_BUILD = 2;
    private static final int LOAD_TYPE_MILESTONE = 3;

    public static Intent newIntent(Context context, String namespace, String projectName, String commitSha) {
        Intent intent = new Intent(context, LoadSomeInfoActivity.class);
        intent.putExtra(EXTRA_PROJECT_NAMESPACE, namespace);
        intent.putExtra(EXTRA_PROJECT_NAME, projectName);
        intent.putExtra(EXTRA_COMMIT_SHA, commitSha);
        intent.putExtra(EXTRA_LOAD_TYPE, LOAD_TYPE_DIFF);
        return intent;
    }

    public static Intent newMergeRequestIntent(Context context, String namespace, String projectName, String mergeRequestId) {
        Intent intent = new Intent(context, LoadSomeInfoActivity.class);
        intent.putExtra(EXTRA_PROJECT_NAMESPACE, namespace);
        intent.putExtra(EXTRA_PROJECT_NAME, projectName);
        intent.putExtra(EXTRA_MERGE_REQUEST, mergeRequestId);
        intent.putExtra(EXTRA_LOAD_TYPE, LOAD_TYPE_MERGE_REQUEST);
        return intent;
    }

    public static Intent newBuildIntent(Context context, String namespace, String projectName, long buildId) {
        Intent intent = new Intent(context, LoadSomeInfoActivity.class);
        intent.putExtra(EXTRA_PROJECT_NAMESPACE, namespace);
        intent.putExtra(EXTRA_PROJECT_NAME, projectName);
        intent.putExtra(EXTRA_BUILD_ID, buildId);
        intent.putExtra(EXTRA_LOAD_TYPE, LOAD_TYPE_BUILD);
        return intent;
    }

    public static Intent newMilestoneIntent(Context context, String namespace, String projectName, String milestoneIid) {
        Intent intent = new Intent(context, LoadSomeInfoActivity.class);
        intent.putExtra(EXTRA_PROJECT_NAMESPACE, namespace);
        intent.putExtra(EXTRA_PROJECT_NAME, projectName);
        intent.putExtra(EXTRA_MILESTONE_ID, milestoneIid);
        intent.putExtra(EXTRA_LOAD_TYPE, LOAD_TYPE_MILESTONE);
        return intent;
    }

    @BindView(R.id.progress)
    View mProgress;

    private int mLoadType;

    private Project mProject;

    @OnClick(R.id.root)
    void onRootClicked() {
        finish();
    }

    private final EasyCallback<Project> mProjectCallback = new EasyCallback<Project>() {
        @Override
        public void success(@NonNull Project response) {
            mProject = response;
            switch (mLoadType) {
                case LOAD_TYPE_DIFF:
                    String sha = getIntent().getStringExtra(EXTRA_COMMIT_SHA);
                    App.instance().getGitLab().getCommit(response.getId(), sha).enqueue(mCommitCallback);
                    return;
                case LOAD_TYPE_MERGE_REQUEST:
                    String mergeRequestId = getIntent().getStringExtra(EXTRA_MERGE_REQUEST);
                    App.instance().getGitLab().getMergeRequestsByIid(response.getId(), mergeRequestId).enqueue(mMergeRequestCallback);
                    return;
                case LOAD_TYPE_BUILD:
                    long buildId = getIntent().getLongExtra(EXTRA_BUILD_ID, -1);
                    App.instance().getGitLab().getBuild(response.getId(), buildId).enqueue(mBuildCallback);
                    return;
                case LOAD_TYPE_MILESTONE:
                    String milestoneId = getIntent().getStringExtra(EXTRA_MILESTONE_ID);
                    App.instance().getGitLab().getMilestonesByIid(response.getId(), milestoneId).enqueue(mMilestoneCallback);
                    return;
            }

        }

        @Override
        public void failure(Throwable t) {
            Timber.e(t, null);
            onError();
        }
    };

    private final EasyCallback<RepositoryCommit> mCommitCallback = new EasyCallback<RepositoryCommit>() {
        @Override
        public void success(@NonNull RepositoryCommit response) {
            Navigator.navigateToDiffActivity(LoadSomeInfoActivity.this, mProject, response);
            finish();
        }

        @Override
        public void failure(Throwable t) {
            Timber.e(t, null);
            onError();
        }
    };

    private final EasyCallback<List<MergeRequest>> mMergeRequestCallback = new EasyCallback<List<MergeRequest>>() {
        @Override
        public void success(@NonNull List<MergeRequest> response) {
            if (!response.isEmpty()) {
                Navigator.navigateToMergeRequest(LoadSomeInfoActivity.this, mProject, response.get(0));
                finish();
            } else {
                onError();
            }
        }

        @Override
        public void failure(Throwable t) {
            Timber.e(t, null);
            onError();
        }
    };

    private final EasyCallback<Build> mBuildCallback = new EasyCallback<Build>() {
        @Override
        public void success(@NonNull Build response) {
            Navigator.navigateToBuild(LoadSomeInfoActivity.this, mProject, response);
            finish();
        }

        @Override
        public void failure(Throwable t) {
            Timber.e(t, null);
            onError();
        }
    };

    private final EasyCallback<List<Milestone>> mMilestoneCallback = new EasyCallback<List<Milestone>>() {
        @Override
        public void success(@NonNull List<Milestone> response) {
            if (!response.isEmpty()) {
                Navigator.navigateToMilestone(LoadSomeInfoActivity.this, mProject, response.get(0));
                finish();
            } else {
                onError();
            }
        }

        @Override
        public void failure(Throwable t) {
            Timber.e(t, null);
            onError();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        ButterKnife.bind(this);
        mProgress.setVisibility(View.VISIBLE);
        mLoadType = getIntent().getIntExtra(EXTRA_LOAD_TYPE, -1);
        Timber.d("Loading some info type: %d", mLoadType);

        switch (mLoadType) {
            case LOAD_TYPE_DIFF:
            case LOAD_TYPE_MERGE_REQUEST:
            case LOAD_TYPE_BUILD:
            case LOAD_TYPE_MILESTONE:
                String namespace = getIntent().getStringExtra(EXTRA_PROJECT_NAMESPACE);
                String project = getIntent().getStringExtra(EXTRA_PROJECT_NAME);
                App.instance().getGitLab().getProject(namespace, project).enqueue(mProjectCallback);
                break;
        }
    }

    private void onError() {
        Toast.makeText(LoadSomeInfoActivity.this, R.string.failed_to_load, Toast.LENGTH_SHORT)
                .show();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.do_nothing, R.anim.fade_out);
    }
}
